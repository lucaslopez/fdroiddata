Categories:Internet
License:GPL-3.0
Web Site:https://mastodon.social/
Source Code:https://github.com/Vavassor/Tusky
Issue Tracker:https://github.com/Vavassor/Tusky/issues

Auto Name:Tusky
Summary:Share words, photos, and videos with others
Description:
Tusky is a client for [https://mastodon.social/ Mastodon], a free and
open-source social network server. Share words, photos, and videos with folks!

Mastodon is decentralized, meaning anyone can run Mastodon and participate in
the social network seamlessly.
.

Repo Type:git
Repo:https://github.com/Vavassor/Tusky

Build:1.0.0-alpha.5,5
    commit=v1.0.0-alpha.5
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.0-alpha.5
Current Version Code:5
