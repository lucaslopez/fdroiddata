Categories:Games
License:GPL-2.0
Web Site:
Source Code:https://github.com/knilch0r/freeshisen
Issue Tracker:https://github.com/knilch0r/freeshisen/issues

Auto Name:FreeShisen
Summary:Shisen-Sho game
Description:
Shisen-Sho is a Japanese tile-based game which uses Mahjong tiles. The objective
is to match similar tiles in pairs until every tile has been removed.
.

Repo Type:git
Repo:https://github.com/knilch0r/freeshisen

Build:0.3,7
    commit=b4f0ce3614b2

Build:0.5,9
    commit=78687484bc4540b5

Build:0.6rc1,10
    commit=269ac2820ad142a9ed65d99585e2334a1d6c1f21
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.6rc1
Current Version Code:10
